package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<Movie, CreateMovieDto> {

    public Movie mapToEntity(CreateMovieDto createMovieDto) {
        Movie movie = new Movie.Builder()
                            .image(createMovieDto.getImage())
                            .title(createMovieDto.getTitle())
                            .year(createMovieDto.getYear())
                            .videoId(createMovieDto.getVideoId())
                            .build();

        return movie;
    }

    @Override
    public Movie convert(CreateMovieDto from) {
        return new Movie();
    }
}
