package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.DetailsMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieDetailsMapper implements Converter<DetailsMovieDto, MovieDto> {

    public DetailsMovieDto mapToDto(Movie movie) {

        DetailsMovieDto detailsMovieDto = new DetailsMovieDto.Builder()
                                                .image(movie.getImage())
                                                .title(movie.getTitle())
                                                .year(movie.getYear())
                                                .build();

        return detailsMovieDto;
    }

    @Override
    public DetailsMovieDto convert(MovieDto from) {
        return new DetailsMovieDto();
    }
}
