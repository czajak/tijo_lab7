package pl.edu.pwsztar.converter;

public interface Converter<T,F> {
    T convert (F from);
}
